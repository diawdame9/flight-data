Description:

    Sur ce projet j'ai intégré le web service Browse Routes du lien https://rapidapi.com/skyscanner/api/skyscanner-flight-search
    Ce WS nous envoit les details des vols disponibles pour une destination suivant une date depart et une date d'arrivée 

Choix technologique
    - Ce projet a ete developpé avec le framework spring boot. 
    - Pour la conection entre notre systeme et le ws du partenaire jai utilisé le client http Okhttp.
    - J'ai ajouté une couche securité(une authentification basic) avec spring security.
    - Pour la documentation api swagger
    - Outils de versionning git

Donnees de test
    Port: 8080
    cred pour l'auth basic: Login = admin, pwd = password
    lien documentation: http://localhost:8080/swagger-ui/index.html
    url endpoint: http://localhost:8080/flight-data/api/v1.0/search

