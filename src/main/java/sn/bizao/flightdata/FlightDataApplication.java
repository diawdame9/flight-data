package sn.bizao.flightdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class FlightDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightDataApplication.class, args);
	}

	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2).select()
//				.paths(PathSelectors.ant("/flight-data/api/v1.1*"))
//				.apis(RequestHandlerSelectors.basePackage("sn.bizao.flightdata"))
				.build()
				.apiInfo(apiDetails());
	}
	
	private ApiInfo apiDetails() {
		
		return new ApiInfo("Flight data", "Flight data", "1.0", "termsOfServiceUrl", new Contact("dame diaw", "https://", "test@test.com"), "Apache 2.0","http://www.apache.org/licenses/LICENSE-2.0",  new ArrayList<>());
		
	}
}
