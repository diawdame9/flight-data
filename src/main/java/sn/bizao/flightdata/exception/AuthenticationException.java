package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class AuthenticationException extends BusinessException {
	public AuthenticationException(String message) {
        super(401, message);
    }
}
