package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class ServiceException extends BusinessException {
	public ServiceException(int code, String message) {
        super(code, message);
    }
}
