package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class IntegrityException extends BusinessException {
	public IntegrityException(int code, String message) {
        super(code, message);
    }
}
