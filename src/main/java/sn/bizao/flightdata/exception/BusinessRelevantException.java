package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.MULTIPLE_CHOICES)
public class BusinessRelevantException extends BusinessException {
	public BusinessRelevantException(String message) {
        super(300, message);
    }
}
