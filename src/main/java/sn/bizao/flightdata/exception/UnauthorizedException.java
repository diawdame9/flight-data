package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends BusinessException{

	public UnauthorizedException(Integer code, String message) {
		super(code, message);
	}

}
