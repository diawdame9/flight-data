package sn.bizao.flightdata.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class TreatmentException extends BusinessException{

	public TreatmentException(Integer code, String message) {
		super(code, message);
	}

}
