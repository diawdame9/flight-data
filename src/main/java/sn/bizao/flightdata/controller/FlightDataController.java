package sn.bizao.flightdata.controller;

import java.io.IOException;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import sn.bizao.flightdata.models.BrowseRoutesRequest;
import sn.bizao.flightdata.models.BrowseRoutesResponse;
import sn.bizao.flightdata.services.HttpService;

@RestController
@RequestMapping({ "/flight-data/api/v1.0" })
public class FlightDataController {

//	@ApiOperation(value = "API for test", notes = "Allow us to test is service is up", response = String.class)
//	@GetMapping(path = "/test1")
//	public String home() {
//		return "Hello world";
//	}
//	
	@ApiOperation(value = "Rrowse routes", notes = "Retrieve the cheapest routes from our cache prices. Similar to the Browse Quotes API but with the routes built for you from the individual quotes.", response = BrowseRoutesResponse.class)
	@PostMapping(path = "/browseroutes", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody BrowseRoutesResponse browseroutes(@RequestBody BrowseRoutesRequest request) throws IOException {
		String country = request.getCountry();
		String currency = request.getCurrency();
		String locale = request.getLocale();
		String originplace = request.getOriginplace();
		String destinationplace = request.getDestinationplace();
		String outboundpartialdate = request.getOutboundpartialdate();
		String inboundpartialdate = request.getInboundpartialdate();
		String url = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/"+country+"/"+currency+"/"+locale+"/"+originplace+"/"+destinationplace+"/"+outboundpartialdate;
		if (inboundpartialdate != null && !inboundpartialdate.isEmpty()) {
			url = url + "/"+inboundpartialdate;
		}
		String response = HttpService.sendGetRequest(url);
		BrowseRoutesResponse rep = new Gson().fromJson(response, BrowseRoutesResponse.class);
		return rep;
	}

}