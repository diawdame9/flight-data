package sn.bizao.flightdata.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RoutesModel {

	
	@SerializedName("Price")
	private Integer price;
	@SerializedName("QuoteDateTime")
	private String quoteDateTime;
	@SerializedName("OriginId")
	private Integer originId;
	@SerializedName("DestinationId")
	private Integer destinationId;
	@SerializedName("QuoteIds")
	private List<Integer> quoteIds;
	
	
	public RoutesModel() {
		super();
	}


	public Integer getPrice() {
		return price;
	}


	public void setPrice(Integer price) {
		this.price = price;
	}


	public String getQuoteDateTime() {
		return quoteDateTime;
	}


	public void setQuoteDateTime(String quoteDateTime) {
		this.quoteDateTime = quoteDateTime;
	}


	public Integer getOriginId() {
		return originId;
	}


	public void setOriginId(Integer originId) {
		this.originId = originId;
	}


	public Integer getDestinationId() {
		return destinationId;
	}


	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}


	public List<Integer> getQuoteIds() {
		return quoteIds;
	}


	public void setQuoteIds(List<Integer> quoteIds) {
		this.quoteIds = quoteIds;
	}

}
