package sn.bizao.flightdata.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

public class BrowseRoutesResponse {

	@ApiModelProperty(notes = "List of quote")
	@SerializedName("Quotes")
	private List<QuotesModel> quotes;
	@ApiModelProperty(notes = "List of carrier")
	@SerializedName("Carriers")
	private List<CarriersModel> carriers;
	@ApiModelProperty(notes = "List of Place")
	@SerializedName("Places")
	private List<PlacesModel> places;
	@ApiModelProperty(notes = "List of currencie")
	@SerializedName("Currencies")
	private List<CurrenciesModel> currencies;
	@ApiModelProperty(notes = "List of route")
	@SerializedName("Routes")
	private List<RoutesModel> routes;
	
	public BrowseRoutesResponse() {
		super();
	}

	public List<QuotesModel> getQuotes() {
		return quotes;
	}

	public void setQuotes(List<QuotesModel> quotes) {
		this.quotes = quotes;
	}

	public List<CarriersModel> getCarriers() {
		return carriers;
	}

	public void setCarriers(List<CarriersModel> carriers) {
		this.carriers = carriers;
	}

	public List<PlacesModel> getPlaces() {
		return places;
	}

	public void setPlaces(List<PlacesModel> places) {
		this.places = places;
	}

	public List<CurrenciesModel> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<CurrenciesModel> currencies) {
		this.currencies = currencies;
	}

	public List<RoutesModel> getRoutes() {
		return routes;
	}

	public void setRoutes(List<RoutesModel> routes) {
		this.routes = routes;
	}

	
}
