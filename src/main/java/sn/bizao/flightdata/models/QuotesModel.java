package sn.bizao.flightdata.models;

import com.google.gson.annotations.SerializedName;

public class QuotesModel {
	
	@SerializedName("QuoteId")
	private Integer quoteId;
	@SerializedName("MinPrice")
	private Integer minPrice;
	@SerializedName("Direct")
	private Boolean direct;
	@SerializedName("OutboundLeg")
	private OutboundLegModel outboundLeg;
	@SerializedName("QuoteDateTime")
	private String quoteDateTime;
	
	public QuotesModel() {
		super();
	}

	public Integer getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}

	public Integer getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Integer minPrice) {
		this.minPrice = minPrice;
	}

	public Boolean getDirect() {
		return direct;
	}

	public void setDirect(Boolean direct) {
		this.direct = direct;
	}

	public OutboundLegModel getOutboundLeg() {
		return outboundLeg;
	}

	public void setOutboundLeg(OutboundLegModel outboundLeg) {
		this.outboundLeg = outboundLeg;
	}

	public String getQuoteDateTime() {
		return quoteDateTime;
	}

	public void setQuoteDateTime(String quoteDateTime) {
		this.quoteDateTime = quoteDateTime;
	}
	
}
