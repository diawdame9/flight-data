package sn.bizao.flightdata.models;

import io.swagger.annotations.ApiModelProperty;

public class BrowseRoutesRequest {

	@ApiModelProperty(notes = "The market country your user is in", required = true, example = "US")         
	private String country;
	@ApiModelProperty(notes = "The currency you want the prices in", required = true, example = "USD")         
	private String currency;
	@ApiModelProperty(notes = "The locale you want the results in (ISO locale)", required = true, example = "en-US")         
	private String locale;
	@ApiModelProperty(notes = "The origin place (see places)", required = true, example = "SFO-sky")         
	private String originplace;
	@ApiModelProperty(notes = "The destination place (see places)", required = true, example = "ORD-sky")         
	private String destinationplace;
	@ApiModelProperty(notes = "The outbound date. Format “yyyy-mm-dd”, “yyyy-mm” or “anytime”.", required = true, example = "anytime")         
	private String outboundpartialdate;
	@ApiModelProperty(notes = "The return date. Format “yyyy-mm-dd”, “yyyy-mm” or “anytime”. Use empty string for oneway trip.", example = "anytime")         
	private String inboundpartialdate;

	public BrowseRoutesRequest() {
		super();
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getOriginplace() {
		return originplace;
	}

	public void setOriginplace(String originplace) {
		this.originplace = originplace;
	}

	public String getDestinationplace() {
		return destinationplace;
	}

	public void setDestinationplace(String destinationplace) {
		this.destinationplace = destinationplace;
	}

	public String getOutboundpartialdate() {
		return outboundpartialdate;
	}

	public void setOutboundpartialdate(String outboundpartialdate) {
		this.outboundpartialdate = outboundpartialdate;
	}

	public String getInboundpartialdate() {
		return inboundpartialdate;
	}

	public void setInboundpartialdate(String inboundpartialdate) {
		this.inboundpartialdate = inboundpartialdate;
	}

}
