package sn.bizao.flightdata.services;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import sn.bizao.flightdata.exception.BadRequestException;
import sn.bizao.flightdata.exception.InternalServerException;
import sn.bizao.flightdata.exception.NotFoundException;
import sn.bizao.flightdata.exception.UnauthorizedException;

public class HttpService {
	static Logger logger = Logger.getLogger(HttpService.class.getName());
	public static String sendGetRequest(String url) throws IOException {
		OkHttpClient client = new OkHttpClient();
		logger.info(url);
		Request request = new Request.Builder().url(url).get()
				.addHeader("x-rapidapi-key", "d4f09b8223msh07792d9442276dfp16767ajsn6eea9205ef17")
				.addHeader("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com").build();

		Response response = client.newCall(request).execute();
		Integer code = response.code();
		String rep = response.body().string();
		logger.info(code+"");
		logger.info(rep);
		if (code == 200) {
			return rep;
		} else if (code == 401) {
			throw new UnauthorizedException(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.name());
		} else if (code == 404) {
			throw new NotFoundException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name());
		} else if (code == 400) {
			throw new BadRequestException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name());
		} else {
			throw new InternalServerException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
					HttpStatus.INTERNAL_SERVER_ERROR.name());
		}

	}

	public static void main(String[] args) {
		try {
			String rep = sendGetRequest(
					"https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/US/USD/en-US/SFO-sky/ORD-sky/anytime");
			System.out.println(rep);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
