package sn.bizao.flightdata;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

@RunWith(SpringRunner.class)
@SpringBootTest
class FlightDataApplicationTests {
	@Autowired
	private WebApplicationContext context;
	@Autowired
	private Filter springSecurityFilterChain;
	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).addFilters(springSecurityFilterChain).build();
	}
	@Test
	public void contextLoads() {
		
	}

	@Test
	public void test_bad_cred() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(context)
				.addFilters(springSecurityFilterChain).build();
		MockMvcBuilders.webAppContextSetup(this.context)
		.apply(SecurityMockMvcConfigurers.springSecurity()).build();
		mvc.perform(get("/").with(httpBasic("test1", "password"))).andDo(print())
				.andExpect(status().isUnauthorized()).andReturn();
	}
	
	@Test
	public void test_valid_cred() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(context)
				.addFilters(springSecurityFilterChain).build();
		MockMvcBuilders.webAppContextSetup(this.context).
		apply(SecurityMockMvcConfigurers.springSecurity()).build();
		mvc.perform(post("/flight-data/api/v1.0/browseroutes")
				.contentType(MediaType.APPLICATION_JSON)
				 .content("{\n" + 
				 		"    \"country\": \"US\",\n" + 
				 		"    \"currency\": \"USD\",\n" + 
				 		"    \"locale\": \"en-US\",\n" + 
				 		"    \"originplace\": \"SFO-sky\",\n" + 
				 		"    \"destinationplace\": \"ORD-sky\",\n" + 
				 		"    \"outboundpartialdate\": \"anytime\",\n" + 
				 		"    \"inboundpartialdate\": \"anytime\"\n" + 
				 		"}\n" + 
				 		"")
				.with(httpBasic("admin", "password"))).andDo(print())
				.andExpect(status().isOk()).andReturn();
	}
}
